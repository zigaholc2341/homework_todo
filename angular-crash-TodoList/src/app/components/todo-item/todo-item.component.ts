import { Component, OnInit, Input,EventEmitter, Output } from '@angular/core';
import { TodoService } from '../../services/todo.service';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoService:TodoService) { }

  ngOnInit(): void {
  }

  setImportants(){
    let importants ={
      todo: true,
      'highlightText': this.todo.important
    }
    return importants;
  }

  //set dynamic classes
setClasses(){
  let classes = {
    todo: true,
    'is-complete': this.todo.completed
  }
  return classes;
}

onToggle(todo){
  //Toggle in UI
  todo.completed = !todo.completed;
  //Toggle on server
  this.todoService.toggleCompleted(todo).subscribe(todo => console.log(todo))
}

//on DELET
onDelete(todo){
  this.deleteTodo.emit(todo);
}

//on Important
onImp(todo){
  todo.important = !todo.important;
  this.todoService.toggleImportant(todo).subscribe(todo => console.log(todo))
}

}
