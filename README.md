<h1> Homework_todo </h1>

<h2> This is a simple Todo List project. </h2>

<h3> With this project you can: </h3>

*  Register, Login, Logout
*  Registered users have control over other registered users
*  Registered users can use todo list
*  With todo list you can create a task, delete it and highlight it as important

<h3>I published my homework on my school server, unfortunately it is only avaible to students and professors of my university. </h3>

<a href="https://www.studenti.famnit.upr.si/~89161115/TodoList/">LINK</a>